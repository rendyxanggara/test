<?php

namespace Tests\TestingTraits;

use Carbon\Carbon;
use App\Models\User;
use Firebase\JWT\JWT;

trait AuthCase
{
    //return json from authentication
    public function getAuthenticate(string $username,string $password)
    {
        $oauthApproval = $this->post('/api/oauth/token', [
            'grant_type' => 'password',
            'client_id' => '2',
            'client_secret' => 'KNuXBExRGDHOvL5uUmEy8hcxAzPuyQYP6EfQdAal',
            'username' => $username,
            'password' => $password,
        ])->json();

        return $oauthApproval;
    }
}
