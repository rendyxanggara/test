<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Tests\TestingTraits\AuthCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use AuthCase;

    /** @test */
    public function basic_user_can_not_access_user_list()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('user.index', [
            'sort_field' => 'id',
            'sort_order' => 'desc'
        ]));

        $response->assertForbidden();
    }

    /** @test */
    public function basic_user_can_not_access_user_detail()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('user.show', 1));

        $response->assertForbidden();
    }

    /** @test */
    public function basic_user_can_not_store_user()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('user.store'));

        $response->assertForbidden();
    }

    /** @test */
    public function basic_user_can_not_update_user()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->putJson(route('user.update', 1));

        $response->assertForbidden();
    }

    /** @test */
    public function admin_user_can_access_user_list()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('user.index'));

        $response->assertOk();
        $this->assertEquals(2, $response->json()['data']['total']);
    }

    /** @test */
    public function admin_user_can_access_user_detail()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('user.show', 1));

        $response->assertOk();
    }

    /** @test */
    public function admin_user_can_store_user()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('user.store'), [
            'name' => 'Rendy',
            'email' => 'user1234@gmail.com',
            'password' => 'user1234',
            'level' => '2',
        ]);

        $response->assertOk();
    }

    /** @test */
    public function admin_user_can_update_user()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->putJson(route('user.update', 1), [
            'name' => 'text basara',
        ]);

        $response->assertOk();
        $user = User::find(1);

        $this->assertEquals('text basara', $user->name);
    }
}
