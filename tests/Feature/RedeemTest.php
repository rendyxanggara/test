<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Tests\TestingTraits\AuthCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RedeemTest extends TestCase
{
    use AuthCase;

    /** @test */
    public function user_can_redeem_gift()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('api.redeem', 2), [
            'quantity' => 1
        ]);

        Transaction::find(4)->delete();
        TransactionItem::where('transaction_id', 4)->delete();

        $response->assertOk();
    }

    /** @test */
    public function user_can_not_redeem_gift_if_quantity_is_insufficient()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('api.redeem', 2), [
            'quantity' => 1001
        ]);

        $response->assertStatus(400);
    }
}
