<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Gift;
use Tests\TestingTraits\AuthCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GiftTest extends TestCase
{
    use AuthCase;
    /** @test */
    public function basic_user_can_access_gift_list()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('gift.index'));

        $response->assertOk();
    }

    /** @test */
    public function basic_user_can_access_filter_rating_on_gift_list()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('gift.index', [
            'rating' => 3
        ]));

        $this->assertEquals(2, $response->json()['data']['total']);
        $response->assertOk();
    }

    /** @test */
    public function basic_user_can_access_sort_on_gift_list_based_on_newest_entry()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('gift.index', [
            'sort_field' => 'id',
            'sort_order' => 'desc'
        ]));

        $this->assertEquals(3, $response->json()['data']['total']);
        $this->assertEquals(3, $response->json()['data']['data']['0']['id']);
        $response->assertOk();
    }

    /** @test */
    public function basic_user_can_access_gift_detail()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('gift.show', 1));

        $response->assertOk();
    }

    /** @test */
    public function basic_user_can_not_store_gift()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('gift.store'));

        $response->assertForbidden();
    }

    /** @test */
    public function basic_user_can_not_update_gift()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->putJson(route('gift.update', 1));

        $response->assertForbidden();
    }

    /** @test */
    public function basic_user_can_not_delete_gift()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->deleteJson(route('gift.destroy', 1));

        $response->assertForbidden();
    }

    /** @test */
    public function admin_user_can_access_gift_list()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('gift.index'));

        $response->assertOk();
    }

    /** @test */
    public function admin_user_can_access_filter_rating_on_gift_list()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('gift.index', [
            'rating' => 3,
        ]));

        $this->assertEquals(2, $response->json()['data']['total']);
        $response->assertOk();
    }

    /** @test */
    public function admin_user_can_access_sort_on_gift_list_based_on_newest_entry()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('gift.index', [
            'sort_field' => 'id',
            'sort_order' => 'desc'
        ]));

        $this->assertEquals(3, $response->json()['data']['total']);
        $this->assertEquals(3, $response->json()['data']['data']['0']['id']);
        $response->assertOk();
    }

    /** @test */
    public function admin_user_can_access_gift_detail()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('gift.show', 1));

        $response->assertOk();
    }

    /** @test */
    public function admin_user_can_store_gift()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('gift.store'), [
            'description' => 'text',
            'price' => 100000,
            'status' => 1,
            'stock' => 100000,
        ]);

        $response->assertOk();
    }

    /** @test */
    public function admin_user_can_update_gift()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->putJson(route('gift.update', 1), [
            'description' => 'text basara',
            'price' => 500000,
            'status' => 99,
            'stock' => 0,
        ]);

        $response->assertOk();
        $gift = Gift::find(1);

        $this->assertEquals(500000, $gift->price);
        $this->assertEquals(99, $gift->status);
        $this->assertEquals(0, $gift->stock);
    }

    /** @test */
    public function admin_user_can_delete_gift()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->deleteJson(route('gift.update', 1), [
            'description' => 'text basara',
            'price' => 500000,
            'status' => 99,
            'stock' => 0,
        ]);

        $response->assertOk();
        $gift = Gift::where('id', 1)->count();

        $this->assertEquals(0, $gift);
    }
}
