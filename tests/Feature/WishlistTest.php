<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\TestingTraits\AuthCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WishlistTest extends TestCase
{
    use AuthCase;

    /** @test */
    public function user_can_wishlist_gift()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('api.wishlist', 4));

        $response->assertOk();
        $this->assertNotNull(auth()->user()->wishlists->count());
    }

    /** @test */
    public function user_can_wishlist_in_existence_gift()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('api.wishlist', 40));

        $response->assertNotFound();
    }
}
