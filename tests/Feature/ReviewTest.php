<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Review;
use Tests\TestingTraits\AuthCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReviewTest extends TestCase
{
    use AuthCase;
    /** @test */
    public function user_can_access_review_list()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('review.index'));

        $response->assertOk();
    }

    /** @test */
    public function user_can_access_sort_on_review_list_based_on_newest_entry()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('review.index', [
            'sort_field' => 'id',
            'sort_order' => 'desc'
        ]));

        $this->assertEquals(3, $response->json()['data']['total']);
        $this->assertEquals(3, $response->json()['data']['data']['0']['id']);
        $response->assertOk();
    }

    /** @test */
    public function user_can_not_store_review_in_another_user_item()
    {
        $token = $this->getAuthenticate('admin@gmail.com', 'admin')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('review.store', 1), [
            'description' => 'text',
            'rate' => 1,
        ]);

        $response->assertForbidden();
    }

    /** @test */
    public function user_can_store_review()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('review.store', 1), [
            'description' => 'text',
            'rate' => 1,
        ]);

        $response->assertOk();
    }

    /** @test */
    public function user_can_not_store_review_in_reviewed_item()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->postJson(route('review.store', 3), [
            'description' => 'text',
            'rate' => 1,
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function user_can_access_review_detail()
    {
        $token = $this->getAuthenticate('user@gmail.com', 'user')['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->getJson(route('review.show', 1));

        $response->assertOk();
    }
}
