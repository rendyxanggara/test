<?php

use Illuminate\Http\Request;
use App\Http\Controllers\RedeemItem;
use App\Http\Controllers\ReviewItem;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WishlistItem;
use App\Http\Controllers\GiftController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ReviewController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('mother', function () {
    return 'mother';
});

Route::middleware('auth:api')->group(function () {
    Route::resource('gift', GiftController::class);
    Route::resource('user', UserController::class);
    Route::get('review', [ReviewController::class, 'index'])->name('review.index');
    Route::get('review/{id}', [ReviewController::class, 'show'])->name('review.show');
    Route::post('wishlist/{productId}', WishlistItem::class)->name('api.wishlist');
    Route::post('redeem/{productId}', RedeemItem::class)->name('api.redeem');
    Route::post('review/item/{itemId}', ReviewItem::class)->name('review.store');
});

