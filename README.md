<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Setup

### Clone Repository
``
git clone git@bitbucket.org:rendyxanggara/test.git
``

### Setup Environment Aplikasi
``
 cp .env.example .env
``

### Setup Environment Unit Testing
```
 cp .env.testing.example .env.testing
```

### Setup XmlPhpunit Unit Testing
``
 cp phpunit.xml.example phpunit.xml.testing
``

### Setup DB Aplikasi
``
 php artisan migrate && php artisan db:seed
``

### Setup DB Unit Testing
``
 php artisan migrate --env=testing && php artisan db:seed --class=UnitTestingSeeder --env=testing
``

### Run Unit Testing
``
 php artisan test
``

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
