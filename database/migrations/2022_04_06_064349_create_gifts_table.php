<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->text('description')->nullable();
            $table->text('image')->nullable();
            $table->integer('price')->nullable();
            $table->integer('status')->nullable()->default('99');
            $table->integer('stock')->nullable()->default('0');
            $table->float('rating')->nullable()->default('0');
            $table->enum('honor', ['hot.topic', 'best.selling'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
};
