<?php

namespace Database\Seeders;

use App\Models\Gift;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class GiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 3; $i++) {
            $gift = Gift::create([
                'user_id' => 1,
                'description' => 'text #'.$i+1,
                'image' => null,
                'price' => 1000 * $i,
                'status' => 1,
                'stock' => round(1.3 * ($i+1)),
                'rating' => round((2.5 * $i)/1),
                'honor' => null,
            ]);
        }
    }
}
