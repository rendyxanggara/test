<?php

namespace Database\Seeders;

use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Belum Bisa direview
        $transaction = Transaction::create([
            'user_id' => 2,
            'status' => 1,
            'transaction_code' => '1234567890',
        ]);

        TransactionItem::create([
            'gift_id' => 1,
            'transaction_id' => $transaction->id,
            'item_snapshot_id' => 1,
            'quantity' => 1,
        ]);

        // sudah bisa direview
        $transaction = Transaction::create([
            'user_id' => 2,
            'status' => 2,
            'transaction_code' => '2345678901',
        ]);

        TransactionItem::create([
            'gift_id' => 1,
            'transaction_id' => $transaction->id,
            'item_snapshot_id' => 2,
            'quantity' => 2,
        ]);

        // sudah direview
        $transaction = Transaction::create([
            'user_id' => 2,
            'status' => 3,
            'transaction_code' => '3456789012',
        ]);

        TransactionItem::create([
            'gift_id' => 1,
            'transaction_id' => $transaction->id,
            'item_snapshot_id' => 2,
            'quantity' => 2,
        ]);
    }
}
