<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Gift;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // user 1 admin 1 user biasa
        User::create([
            'name' => 'rendy',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'email_verified_at' => Carbon::now(),
            'level' => 1,
        ]);

        User::create([
            'name' => 'rendy',
            'email' => 'user@gmail.com',
            'password' => Hash::make('user'),
            'email_verified_at' => Carbon::now(),
            'level' => 2,
        ]);

        // gift
        for ($i=0; $i < 10; $i++) {
            $gift = Gift::create([
                'user_id' => 1,
                'description' => 'text #'.$i+1,
                'image' => null,
                'price' => 1000 * $i,
                'status' => 1,
                'stock' => round(1.3 * ($i+1)),
                'rating' => round((2.5 * $i)/1),
                'honor' => null,
            ]);
        }

        // oauth client
        DB::table('oauth_clients')->updateOrInsert(
            [
                'id' => 2,
            ],
            [
                'name' => 'Laravel Password Grant Client',
                'secret' => 'KNuXBExRGDHOvL5uUmEy8hcxAzPuyQYP6EfQdAal',
                'provider' => 'users',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );

    }
}
