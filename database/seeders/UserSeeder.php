<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // admin
        User::create([
            'name' => 'rendy',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'email_verified_at' => Carbon::now(),
            'level' => 1,
        ]);

        // user
        User::create([
            'name' => 'rendy',
            'email' => 'user@gmail.com',
            'password' => Hash::make('user'),
            'email_verified_at' => Carbon::now(),
            'level' => 2,
        ]);
    }
}
