<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\GiftSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\ExampleSeeder;
use Database\Seeders\PassportSeeder;
use Database\Seeders\TransactionSeeder;

class UnitTestingSeeder extends Seeder
{
    /**
     * Seeder for Testing Environment.
     *
     * @return void
     */
    public function run()
    {
        // Run only Testing Environment
        // In Any Situation do not run this Seeder as Production or Staging

        $this->call([
            UserSeeder::class,
            PassportSeeder::class,
            GiftSeeder::class,
            TransactionSeeder::class,
        ]);

        // If developer want to add another Seeder for Unit Testing purpose.
        // Add those here.

        $this->call([
            //
        ]);
    }
}
