<?php

namespace App\Http\Services;

use Carbon\Carbon;
use App\Models\Gift;
use App\Http\Resources\TransactionDefinition;
use App\Http\Repositories\Contracts\TransactionContract;
use App\Http\Repositories\Contracts\ItemSnapshotContract;
use App\Http\Repositories\Contracts\TransactionItemContract;

class Redeem
{
    /** @var TransactionContract */
    protected $transaction;

    /** @var ItemSnapshotContract */
    protected $snapshot;

    /** @var ItTransactionItemContract */
    protected $item;

    public function __construct (TransactionContract $transaction, ItemSnapshotContract $snapshot, TransactionItemContract $item)
    {
        $this->transaction = $transaction;
        $this->snapshot = $snapshot;
        $this->item = $item;
    }


    public function apply($params)
    {
        $params = array_merge($params, [
            'gift_id' => request()->route('productId'),
            'user_id' => auth()->user()->id,
            'transaction_code' => Carbon::now()->format('ymdHis'),
        ]);

        $transactionParams = new TransactionDefinition($params);
        $transaction = $this->transaction->store($transactionParams->getParams());

        $gift = Gift::find($params['gift_id']);

        if (!$gift) {
            abort(400, __('gift.not.found'));
        }

        if ($gift->stock < $params['quantity']) {
            abort(400, __('stock.insufficient'));
        }

        $snapshotParams = [
            'gift_id' => $params['gift_id'],
            'description' => $gift->description,
            'image' => $gift->image,
            'price' => $gift->price,
        ];

        $snapshot = $this->snapshot->store($snapshotParams);

        $itemParams = [
            'gift_id' => $params['gift_id'],
            'transaction_id' => $transaction->id,
            'item_snapshot_id' => $snapshot->id,
            'quantity' => $params['quantity'],
        ];

        $this->item->store($itemParams);

        return $transaction->load('transactionItem.itemSnapshot');
    }
}
