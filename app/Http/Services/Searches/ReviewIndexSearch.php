<?php

namespace App\Http\Services\Searches;

use App\Models\Review;
use App\Models\TransactionItem;
use Illuminate\Database\Eloquent\Model;
use App\Http\Services\Searches\HttpSearch;
use App\Http\Services\Searches\Filters\Review\Sort;

class ReviewIndexSearch extends HttpSearch
{

 	protected function passable()
	{
		return TransactionItem::query()
            ->with('review');
	}

	protected function filters(): array
	{
		return [
            Sort::class
		];
	}

	protected function thenReturn($reviewIndexSearch)
	{
		return $reviewIndexSearch;
	}
}
