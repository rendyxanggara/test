<?php

namespace App\Http\Services\Searches;

use App\Models\Gift;
use Illuminate\Database\Eloquent\Model;
use App\Http\Services\Searches\HttpSearch;
use App\Http\Services\Searches\Filters\Gift\Sort;
use App\Http\Services\Searches\Filters\Gift\Stock;
use App\Http\Services\Searches\Filters\Gift\Rating;

class GiftIndexSearch extends HttpSearch
{

 	protected function passable()
	{
		return Gift::query();
	}

	protected function filters(): array
	{
		return [
            Rating::class,
            Stock::class,
            Sort::class,
		];
	}

	protected function thenReturn($giftIndexSearch)
	{
		return $giftIndexSearch;
	}
}
