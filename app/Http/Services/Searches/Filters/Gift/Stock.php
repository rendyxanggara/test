<?php
namespace App\Http\Services\Searches\Filters\Gift;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Services\Searches\Contracts\FilterContract;

class Stock implements FilterContract
{
	/** @var string|null */
	protected $stock;

	/**
	 * @param string|null $stock
	 * @return void
	 */
	public function __construct($stock)
	{
		$this->stock = $stock;
	}

	/**
	 * @return mixed
	 */
	public function handle(Builder $query, Closure $next)
	{
		if (!$this->keyword()) {
			return $next($query);
		}
		$query->whereNotIn('status', [99]);

		return $next($query);
	}

	/**
	 * Get stock keyword.
	 *
	 * @return mixed
	 */
	protected function keyword()
	{
		if ($this->stock) {
			return $this->stock;
	}

		$this->stock = request('stock', null);

		return request('stock');
	}
}
