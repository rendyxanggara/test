<?php
namespace App\Http\Services\Searches\Filters\Gift;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Services\Searches\Contracts\FilterContract;

class Rating implements FilterContract
{
	/** @var string|null */
	protected $rating;

	/**
	 * @param string|null $rating
	 * @return void
	 */
	public function __construct($rating)
	{
		$this->rating = $rating;
	}

	/**
	 * @return mixed
	 */
	public function handle(Builder $query, Closure $next)
	{
		if (!$this->keyword()) {
			return $next($query);
		}
		$query->where('rating', '>=', $this->rating);

		return $next($query);
	}

	/**
	 * Get rating keyword.
	 *
	 * @return mixed
	 */
	protected function keyword()
	{
		if ($this->rating) {
			return $this->rating;
	}

		$this->rating = request('rating', null);

		return request('rating');
	}
}
