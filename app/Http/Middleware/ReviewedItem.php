<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\TransactionItem;

class ReviewedItem
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $item = TransactionItem::find($request->route('itemId'));

        if ($item->transaction->status == 3) {
            return response()->json([
                'message' => 'Already Reviewed'
            ], 400);
        }

        if ($item->transaction->user_id != auth()->user()->id) {
            return response()->json([
                'message' => 'Not Intended User'
            ], 403);
        }

        return $next($request);
    }
}
