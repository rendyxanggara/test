<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->level != 1) {
            if ($request->headers->get('Accept') == 'application/json') {
                return response()->json([
                    'message' => 'UnAuthorized'
                ], 403);
            } else {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
