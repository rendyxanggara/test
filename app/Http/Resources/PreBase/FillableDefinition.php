<?php

namespace App\Http\Resources\Prebase;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

abstract class FillableDefinition
{
    /** @var array */
    protected $fillableForFile = [
        // ex:file etc.
    ];

     /** @var array */
     protected $params = [];

    /** @var \Illuminate\Database\Eloquent\Model */
    protected $model = null;

    public function __construct($params)
    {
        $this->model = new Model();
        $this->params = $params;
    }

    /**
     * Transform the fillable into an array.
     *
     * @return array
     */
    public function getParams()
    {
        $response = [];

        $fillables = $this->model->getFillable();

        foreach ($this->params as $key => $params) {
            if (in_array($key, $fillables)) {
                $response[$key] = $params;

                if (in_array($key, $this->fillableForFile)) {
                    $response[$key] = $this->getImageUrl($params);
                }
            }
        }

        return $response;
    }

    protected function getImageUrl($file): ?string
    {
        if (!$file) {
            return null;
        }

        if (! ($file instanceof UploadedFile)) {
            return $file;
        }

        $storagePath = $file->store("public/gifts/image");
        $url = Storage::url($storagePath);
        return $url;
    }
}
