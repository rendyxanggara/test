<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Prebase\FillableDefinition;

class UserDefinition extends FillableDefinition
{
    public function __construct($params)
    {
        $this->model = new User();
        $this->params = $params;
    }
}
