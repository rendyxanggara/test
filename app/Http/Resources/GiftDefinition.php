<?php

namespace App\Http\Resources;

use App\Models\Gift;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Prebase\FillableDefinition;

class GiftDefinition extends FillableDefinition
{
    /** @var array */
    protected $fillableForFile = [
        'image'
    ];

    public function __construct($params)
    {
        $this->model = new Gift();
        $this->params = $params;
    }
}
