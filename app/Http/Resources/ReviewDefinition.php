<?php

namespace App\Http\Resources;

use App\Models\Review;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Prebase\FillableDefinition;

class ReviewDefinition extends FillableDefinition
{
    /** @var array */
    protected $fillableForFile = [
        'image'
    ];

    public function __construct($params)
    {
        $this->model = new Review();
        $this->params = $params;
    }
}
