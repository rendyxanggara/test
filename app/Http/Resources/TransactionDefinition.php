<?php

namespace App\Http\Resources;

use App\Models\Transaction;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Prebase\FillableDefinition;

class TransactionDefinition extends FillableDefinition
{
    public function __construct($params)
    {
        $this->model = new Transaction();
        $this->params = $params;
    }
}
