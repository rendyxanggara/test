<?php

namespace App\Http\Controllers;

use App\Models\Gift;
use Illuminate\Http\Request;

class WishlistItem extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $productId)
    {
        $gift = Gift::findOrFail($productId);
        auth()->user()->wishlists()
            ->save($gift);

        return response()->json([
            'message' => __('gift.added.to.wishlist')
        ], 200);
    }
}
