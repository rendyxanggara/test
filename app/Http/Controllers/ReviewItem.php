<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransactionItem;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ReviewDefinition;
use App\Http\Repositories\Contracts\ReviewContract;

class ReviewItem extends Controller
{
    /** @var ReviewContract */
    protected $review;

    public function __construct (ReviewContract $review)
    {
        $this->repository = $review;
        $this->middleware(['reviewed.item']);
    }

    /**
     * Handle the incoming request.
     *
     * @param  integer  $itemId
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $itemId)
    {
        $review = DB::transaction(function () use ($request, $itemId) {
            $fillable = new ReviewDefinition($this->mergedArray($request->all(), $itemId));
            return $this->repository->store($fillable->getParams());
        });

        return response()->json([
            'message' => __('review.created'),
            'review' => $review,
        ]);
    }

    public function mergedArray($params, $itemId)
    {
        $item = TransactionItem::find($itemId);

        $transaction = $item->transaction;
        $transaction->status = 3;
        $transaction->save();

        return array_merge([
            'transaction_item_id' => $itemId,
            'user_id' => auth()->user()->id,
            'gift_id' => $item->gift_id
        ], $params);
    }
}
