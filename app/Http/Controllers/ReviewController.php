<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Repositories\Contracts\ReviewContract;

class ReviewController extends Controller
{
    /** @var ReviewContract */
    protected $review;

    public function __construct (ReviewContract $review)
    {
        $this->repository = $review;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reviews = DB::transaction(function () use ($request) {
            return $this->repository->indexPaginated($request->all());
        });

        return response()->json([
            'message' => $reviews->count() ? __('review.not.empty') : __('review.empty'),
            'data' => $reviews,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = $this->repository->find($id);

        return response()->json([
            'message' => __('review.founded'),
            'review' => $review,
        ]);
    }
}
