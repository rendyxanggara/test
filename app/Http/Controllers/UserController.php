<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserDefinition;
use App\Http\Repositories\Contracts\UserContract;

class UserController extends Controller
{

    /** @var UserContract */
    protected $user;

    public function __construct (UserContract $user)
    {
        $this->user = $user;
        $this->middleware(['admin.only']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = DB::transaction(function () use ($request) {
            return $this->user->indexPaginated($request->all());
        });

        return response()->json([
            'message' => $users->count() ? __('user.not.empty') : __('user.empty'),
            'data' => $users,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->user->find($id);

        return response()->json([
            'message' => __('user.founded'),
            'user' => $user,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = DB::transaction(function () use ($request) {
            $fillable = new UserDefinition($request->all());
            return $this->user->store($fillable->getParams());
        });

        return response()->json([
            'message' => __('user.created'),
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = DB::transaction(function () use ($request, $id) {
            $fillable = new UserDefinition($request->all());
            return $this->user->update($fillable->getParams(), $id);
        });
        return response()->json([
            'message' => __('user.updated'),
            'user' => $this->user->find($id),
        ]);
    }
}
