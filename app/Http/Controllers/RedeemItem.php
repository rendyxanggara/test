<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\Redeem;
use Illuminate\Support\Facades\DB;

class RedeemItem extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $productId)
    {
        DB::transaction(function () use ($request) {
            $redeem = app()->make(Redeem::class);
            $redeem->apply($request->all());
        });

        return response()->json([
            'message' => __('redeem.done'),
        ]);
    }
}
