<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\GiftDefinition;
use App\Http\Repositories\Contracts\GiftContract;

class GiftController extends Controller
{
    /** @var GiftContract */
    protected $gift;

    public function __construct (GiftContract $gift)
    {
        $this->repository = $gift;
        $this->middleware(['admin.only'], ['except' => [
            'index', 'show'
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gifts = DB::transaction(function () use ($request) {
            return $this->repository->indexPaginated($request->all());
        });

        return response()->json([
            'message' => $gifts->count() ? __('gift.not.empty') : __('gift.empty'),
            'data' => $gifts,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gift = $this->repository->find($id);

        return response()->json([
            'message' => __('gift.founded'),
            'gift' => $gift,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gift = DB::transaction(function () use ($request) {
            $fillable = new GiftDefinition($request->all());
            return $this->repository->store($fillable->getParams());
        });

        return response()->json([
            'message' => __('gift.created'),
            'gift' => $gift,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gift = DB::transaction(function () use ($request, $id) {
            $fillable = new GiftDefinition($request->all());
            return $this->repository->update($fillable->getParams(), $id);
        });
        return response()->json([
            'message' => __('gift.updated'),
            'gift' => $this->repository->find($id),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prevGift = clone $this->repository->find($id);

        DB::transaction(function () use ($id) {
            $gift = $this->repository->delete($id);
        });

        return response()->json([
            'message' => __('gift.deleted'),
            'gift' => $prevGift,
        ]);
    }
}
