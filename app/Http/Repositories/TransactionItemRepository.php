<?php

namespace App\Http\Repositories;

use App\Models\TransactionItem;
use App\Http\Repositories\BaseRepository;
use App\Http\Repositories\Contracts\TransactionItemContract;

class TransactionItemRepository extends BaseRepository implements TransactionItemContract
{
	/** @var TransactionItem */
	protected $transactionItem;

	public function __construct(TransactionItem $transactionItem)
	{
		parent::__construct($transactionItem);
		$this->transactionItem = $transactionItem;
	}

	public function customRules()
	{
		return;
	}
}
