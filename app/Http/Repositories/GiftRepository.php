<?php

namespace App\Http\Repositories;

use App\Models\Gift;
use App\Http\Repositories\BaseRepository;
use App\Http\Services\Searches\GiftIndexSearch;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Repositories\Contracts\GiftContract;

class GiftRepository extends BaseRepository implements GiftContract
{
	/** @var Gift */
	protected $gift;

    /** @var GiftIndexSearch */
	protected $search;

	public function __construct(Gift $gift)
	{
		parent::__construct($gift);
		$this->gift = $gift;
        $this->search = app()->make(GiftIndexSearch::class);
	}

    /**
     * @return LengthAwarePaginator
     */
    public function IndexPaginated($params): LengthAwarePaginator
    {
        $perPage = 5;

        if (array_key_exists('per_page', $params)) {
            $perPage = $params['per_page'];
        }

        return $this->search->apply()->paginate($perPage);
    }
}
