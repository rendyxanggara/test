<?php

namespace App\Http\Repositories;

use App\Models\Review;
use App\Http\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Services\Searches\ReviewIndexSearch;
use App\Http\Repositories\Contracts\ReviewContract;

class ReviewRepository extends BaseRepository implements ReviewContract
{
	/** @var Review */
	protected $review;

	public function __construct(Review $review)
	{
		parent::__construct($review);
		$this->review = $review;
        $this->search = app()->make(ReviewIndexSearch::class);
	}

    /**
     * @return LengthAwarePaginator
     */
    public function IndexPaginated($params): LengthAwarePaginator
    {
        $perPage = 5;

        if (array_key_exists('per_page', $params)) {
            $perPage = $params['per_page'];
        }

        return $this->search->apply()->paginate($perPage);
    }
}
