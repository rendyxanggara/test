<?php

namespace App\Http\Repositories;

use App\Models\Transaction;
use App\Http\Repositories\BaseRepository;
use App\Http\Repositories\Contracts\TransactionContract;

class TransactionRepository extends BaseRepository implements TransactionContract
{
	/** @var Transaction */
	protected $transaction;

	public function __construct(Transaction $transaction)
	{
		parent::__construct($transaction);
		$this->transaction = $transaction;
	}

	public function customRules()
	{
		return;
	}
}
