<?php

namespace App\Http\Repositories\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;

interface UserContract
{
	/**
     * @return LengthAwarePaginator
     */
    public function IndexPaginated($params): LengthAwarePaginator;
}
