<?php

namespace App\Http\Repositories\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;

interface ReviewContract
{
	/**
     * @return LengthAwarePaginator
     */
    public function IndexPaginated($params): LengthAwarePaginator;
}
