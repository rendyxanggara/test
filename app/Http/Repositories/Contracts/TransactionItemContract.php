<?php

namespace App\Http\Repositories\Contracts;

interface TransactionItemContract
{
	/**
	 * @return mixed
	 */
	public function customRules();
}