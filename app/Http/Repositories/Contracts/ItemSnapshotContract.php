<?php

namespace App\Http\Repositories\Contracts;

interface ItemSnapshotContract
{
	/**
	 * @return mixed
	 */
	public function customRules();
}