<?php

namespace App\Http\Repositories\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;

interface GiftContract
{
	/**
     * @return LengthAwarePaginator
     */
    public function IndexPaginated($params): LengthAwarePaginator;
}
