<?php

namespace App\Http\Repositories\Contracts;

interface TransactionContract
{
	/**
	 * @return mixed
	 */
	public function customRules();
}