<?php

namespace App\Http\Repositories;

use App\Models\User;
use App\Http\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Repositories\Contracts\UserContract;

class UserRepository extends BaseRepository implements UserContract
{
	/** @var User */
	protected $user;

	public function __construct(User $user)
	{
		parent::__construct($user);
		$this->user = $user;
	}

	    /**
     * @return LengthAwarePaginator
     */
    public function IndexPaginated($params): LengthAwarePaginator
    {
        $perPage = 5;

        if (array_key_exists('per_page', $params)) {
            $perPage = $params['per_page'];
        }

        return $this->user->paginate($perPage);
    }
}
