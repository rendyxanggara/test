<?php

namespace App\Http\Repositories;

use App\Models\ItemSnapshot;
use App\Http\Repositories\BaseRepository;
use App\Http\Repositories\Contracts\ItemSnapshotContract;

class ItemSnapshotRepository extends BaseRepository implements ItemSnapshotContract
{
	/** @var ItemSnapshot */
	protected $itemSnapshot;

	public function __construct(ItemSnapshot $itemSnapshot)
	{
		parent::__construct($itemSnapshot);
		$this->itemSnapshot = $itemSnapshot;
	}

	public function customRules()
	{
		return;
	}
}
