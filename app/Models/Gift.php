<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Review;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Gift extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'user_id',
        'description',
        'image',
        'price',
        'status',
        'stock',
        'rating',
        'honor',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function getHonorAttribute($value)
    {
        $createdDays = Carbon::now()->diffInDays(Carbon::parse($this->created_at));
        if ($createdDays < 7) {
            return __('new.arrival');
        }

        return __($value);
    }

    public function getRatingAttribute($value)
    {
        return round($value);
    }
}
