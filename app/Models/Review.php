<?php

namespace App\Models;

use App\Models\TransactionItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Review extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'user_id',
        'description',
        'image',
        'transaction_item_id',
        'gift_id',
        'rate'
    ];

    public function transactionItem()
    {
        return $this->belongsTo(TransactionItem::class);
    }
}
