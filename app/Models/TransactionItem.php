<?php

namespace App\Models;

use App\Models\Gift;
use App\Models\Review;
use App\Models\Transaction;
use App\Models\ItemSnapshot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TransactionItem extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'gift_id',
        'transaction_id',
        'item_snapshot_id',
        'quantity',
    ];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }

    public function itemSnapshot()
    {
        return $this->belongsTo(ItemSnapshot::class);
    }

    public function review()
    {
        return $this->hasOne(Review::class);
    }
}
