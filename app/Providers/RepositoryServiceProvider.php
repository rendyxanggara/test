<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Repositories\BaseRepository;
use App\Http\Repositories\GiftRepository;
use App\Http\Repositories\UserRepository;
use App\Http\Repositories\ReviewRepository;
use App\Http\Repositories\ExampleRepository;
use App\Http\Repositories\TransactionRepository;
use App\Http\Repositories\Contracts\GiftContract;
use App\Http\Repositories\Contracts\UserContract;
use App\Http\Repositories\ItemSnapshotRepository;
use App\Http\Repositories\Contracts\ReviewContract;
use App\Http\Repositories\Contracts\ExampleContract;
use App\Http\Repositories\TransactionItemRepository;
use App\Http\Repositories\Contracts\TransactionContract;
use App\Http\Repositories\Contracts\ItemSnapshotContract;
use App\Http\Repositories\Contracts\BaseRepositoryContract;
use App\Http\Repositories\Contracts\TransactionItemContract;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(BaseRepositoryContract::class, BaseRepository::class);
        $this->app->bind(ExampleContract::class, ExampleRepository::class);
        $this->app->bind(GiftContract::class, GiftRepository::class);
        $this->app->bind(ReviewContract::class, ReviewRepository::class);
        $this->app->bind(TransactionContract::class, TransactionRepository::class);
        $this->app->bind(TransactionItemContract::class, TransactionItemRepository::class);
        $this->app->bind(ItemSnapshotContract::class, ItemSnapshotRepository::class);
        $this->app->bind(UserContract::class, UserRepository::class);
    }
}
